* Cleanup outstanding idle connections more cleverly -- can we only do
  this cleanup when under memory pressure?  is there a better way to
  choose which connections to kill?  IDLE_TIMEOUT is a pretty clumsy
  knob compared to ideal memory management practices.

* Enable unix-domain sockets in the HTTP_TARGET and DNS_TARGET
  backends -- no reason that these need to be TCP addresses.

* Are there other client-speaks-first streaming protocols that we can
  demux in addition to HTTP and DNS?

* Consider making hddemux work on non-systemd systems (e.g. pass a set
  of listener arguments on the CLI in addition to looking for
  SD_LISTEN_FDS)


