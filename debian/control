Source: hddemux
Section: net
Priority: optional
Maintainer: Daniel Kahn Gillmor <dkg@fifthhorseman.net>
Build-Depends:
 curl [!arm64] <!nocheck>,
 debhelper-compat (= 13),
 gnutls-bin [!arm64] <!nocheck>,
 knot-dnsutils [!arm64] <!nocheck>,
 knot-resolver [!arm64 !s390x] <!nocheck>,
 libsystemd-dev,
 libuv1-dev,
 nginx-light [!arm64] <!nocheck>,
 pandoc,
 pkg-config,
 systemd [!arm64] <!nocheck>,
Standards-Version: 4.6.0
Vcs-Git: https://salsa.debian.org/debian/hddemux.git -b debian/main
Vcs-Browser: https://salsa.debian.org/debian/hddemux
Homepage: https://0xacab.org/dkg/hddemux
Rules-Requires-Root: no

Package: hddemux
Architecture: any
Depends:
 adduser,
 ${misc:Depends},
 ${shlibs:Depends},
Description: HTTP/1.x and DNS demultiplexer
 hddemux listens on a stream and routes incoming clients to either an
 HTTP/1.x backend or a DNS stream-based backend depending on the first
 request to appear on the stream.
 .
 This is useful when making DNS-over-TLS (RFC 7858) connections that
 appear to the network be HTTPS connections, for example, which makes
 it easier to traverse a network that would prefer to block the user
 from making DNS-over-TLS queries.
